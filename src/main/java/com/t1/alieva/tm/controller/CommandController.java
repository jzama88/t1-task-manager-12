package com.t1.alieva.tm.controller;

import com.t1.alieva.tm.api.controller.ICommandController;
import com.t1.alieva.tm.api.service.ICommandService;
import com.t1.alieva.tm.model.Command;
import com.t1.alieva.tm.util.FormatUtil;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showWelcome() {
        System.out.println("**WELCOME to TASK-MANAGER**");
    }

    @Override
    public void showErrorArgument() {
        System.err.println("Error! This argument is not supported...  ");
    }

    @Override
    public void showArguments() {
        System.out.println("ARGUMENTS");
        final Command[] commands = commandService.getTerminalCommands();
        for(final Command command: commands)
        { final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public void showCommands() {
        System.out.println("COMMANDS ");
        final Command[] commands = commandService.getTerminalCommands();
        for(final Command command: commands)
        { final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    @Override
    public void showErrorCommand() {
        System.err.println("Error! This command is not supported...  ");
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.5.0");
    }

    @Override
    public void showInfo() {
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        System.out.println("Free memory (bytes): " + freeMemoryFormat);
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = FormatUtil.formatBytes(maxMemory);
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = maxMemoryCheck ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory (bytes): " + maxMemoryFormat);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        System.out.println("Total memory (bytes): " + totalMemoryFormat);
        final long usageMemory = totalMemory - freeMemory;
        final String usageMemoryFormat = FormatUtil.formatBytes(usageMemory);
        System.out.println("Usage memory (bytes): " + usageMemoryFormat);
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name:Alieva Djamilya");
        System.out.println("E-mail:jzama88@gmail.com");
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for(final Command command: commands) System.out.println(command);
    }
}
