package com.t1.alieva.tm.api.controller;

public interface ICommandController {

    void showWelcome();

    void showErrorArgument();

    void showArguments();

    void showCommands();

    void showErrorCommand();

    void showVersion();

    void showInfo();

    void showAbout();

    void showHelp();
}
